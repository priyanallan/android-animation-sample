package com.lilacgames.learning.androidanimation;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    RelativeLayout relativeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        relativeLayout = (RelativeLayout) findViewById(R.id.activity_main);

        ImageView image1 = new ImageView(getBaseContext());
        image1.setImageResource(R.drawable.shape);
        image1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));

        image1.setX(10);
        image1.setY(30);

        relativeLayout.addView(image1);

//        View[] views = createView();
//        for(View x:views) {
//            relativeLayout.addView(x);
//        }


        System.out.println("width"+relativeLayout.getWidth()+" "+relativeLayout.getHeight());
    }

    private View[] createView() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        System.out.println("screen width"+width);
        int height = size.y;
        System.out.println("screeen height"+height);

        Random r = new Random();



        //Create 3 ImageViews
        View[] imageViews = new View[3];
        for(int i=0;i<3;i++) {
            ImageView image1 = new ImageView(getBaseContext());
            image1.setImageResource(R.drawable.shape);
            image1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
            int x = r.nextInt(width);
            System.out.println("random width"+x);
            int y = r.nextInt(height);
            System.out.println("random heigth"+y);
            image1.setX(0);
            image1.setTop(0);
            imageViews[i] = image1;
        }
        return imageViews;
    }


}
